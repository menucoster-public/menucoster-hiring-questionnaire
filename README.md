# MenuCoster Software Architect / Project Manager Questionnaire

Hi there! Thank you for taking the time to fill out our hiring questionnaire!\
There are a few different sections, containing questions for each subject we think software project leaders should be familiar with.\
Not everybody has all the skills we ask about, so we value honesty when you fill out your answers; we do not expect you to know everything about everything on this list, there is always room to learn.


Please copy the sections below into an email or word doc, fill in your responses, and send them to us via email.


If you’ve applied for a technical role, please also be sure to complete the developer homework test:\
https://gitlab.com/menucoster-public/developer-homework-test 


 
## Agile Methodology Thoughts
For each of the following, please choose which you believe is more important in an agile project:
-	Customer collaboration OR Contract Negotiation
    - *your answer here*
-	Comprehensive Documentation OR Working Software 
    - *your answer here*
-	Individuals and Interactions OR Processes and Tools
    - *your answer here*
-	Following a Plan OR Responding to Change
    - *your answer here*

What do you believe are the major benefits of an agile project methodology as opposed to traditional waterfall?\
*your answer here*

How does CI/CD help or hinder in the enablement of agile methodology?\
*your answer here*

What is the purpose of a user story, and why is it worded in the structure “as a X I want X so that X”\
*your answer here*

What is the difference between Agile Kanban & Agile SCRUM, when are each of them suitable?\
*your answer here*


## Personal / Subjective Experience

What is the largest team size that you have been project or architecture lead for?\
*your answer here*

What was the structure of that team? (eg, 2x full-time developers, 1 x project manager, 1 x qatester)\
*your answer here*

What is your proudest career achievement?\
*your answer here*

What are your goals for the future of your career? Ie, what would you like to do/become in future…\
*your answer here*


## Technical

Have you had much experience with technical architecture diagramming & modelling? What tools or processes do you use to accomplish a technical specification?\
*your answer here*

Have you ever been a scrum master in your team? What are the major responsibilities of that role?\
*your answer here*

If you are working with a product owner to define a user story that will require complex programming and technical implementation, what kind of resources will you compile in order for other developers to implement it? Or, would you rather just do it yourself and not utilize your team.\
*your answer here*



